// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'

import VueResource from 'vue-resource'
Vue.use(VueResource);

import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
Vue.use(Vuetify, {
  theme: {
    primary: '#ef6c00',
    // secondary: '#424242',
    // accent: '#82B1FF',
    // error: '#FF5252',
    // info: '#2196F3',
    // success: '#4CAF50',
    // warning: '#FFC107'
  }
})

import VueMoment from 'vue-moment'
Vue.use(VueMoment);

var VueScrollTo = require('vue-scrollto');
Vue.use(VueScrollTo)

import filters from './filters'
Vue.use(filters);

import services from './services'
Vue.use(services, {
  logger: (builder) => {
    builder.level('all').fields({ date: () => new Date() })
  },
  api: {
    host: 'localhost',
    port: 9000,
    // port: 53229,
    discovery: 'discovery'
  },
  auth: {
    loginUrl: 'http://localhost:53229/account/login'
  }
});

import components from './components'
Vue.use(components);

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: { App }
})
