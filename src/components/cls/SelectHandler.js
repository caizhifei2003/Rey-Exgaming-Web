class SelectItem {
    get item() { return this._item; }
    get name() { return this._item.name; }
    get value() { return this._item.value; }

    constructor(item) {
        this._item = item;
    }
}

class SelectHandler {
    get items() { return this._items; }
    get current() { return this._items.find(x => x.value === this._value); }

    constructor() {
        this._items = [];
        this._value = null;
    }

    initItems(items) {
        this._items = items.map(item => new SelectItem(item));
    }

    initValue(value) {
        this._value = value;
    }

    init(items, value) {
        this._items = items.map(item => new SelectItem(item));
        this._value = value;
    }
}

export default SelectHandler