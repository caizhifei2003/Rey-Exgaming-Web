class CheckItem {
    get item() { return this._item; }
    get name() { return this._item.name; }
    get value() { return this._item.value; }
    get checked() { return this._checked; }
    set checked(val) { this._checked = val; }

    constructor(item, checked) {
        this._item = item;
        this._checked = checked;
    }
}

export default CheckItem