import CheckItem from './CheckItem'

class CheckHandler {
    get items() { return this._items; }
    get checkedItmes() { return this._items.filter(x => x.checked).map(x => x.item); }

    constructor() {
        this._items = [];
        this._values = [];
    }

    initItems(items) {
        this._items = items.map(item => new CheckItem(item, this._values.findIndex(val => item.value === val) > -1));
    }

    initValues(values) {
        this._items.forEach(x => {
            const checked = values.findIndex(val => x.item.value === val) > -1;
            if (x.checked !== checked) {
                x.checked = checked;
            }
        });
    }

    init(items, values) {
        this._items = items.map(item => new CheckItem(item, values.findIndex(val => item.value === val) > -1));
        this._values = values;
    }
}

export default CheckHandler