import Plugin from '../core/Plugin'
import CheckFilter from './CheckFilter'
import SelectOrder from './SelectOrder'

export default new Plugin()
    .addComponent('rey-check-filter', CheckFilter)
    .addComponent('rey-select-order', SelectOrder)