import Vue from 'vue'
import _ from 'lodash'
import moment from 'moment';
import { MongoJsonEncoder, Base64Encoder } from '../core/encoding'

class DiscoverApi {
    get discovered() { return this._discovered; }

    constructor(api) {
        this._discovered = false;
        this._api = api;
        this._jsonEncoder = new MongoJsonEncoder();
        this._base64Encoder = new Base64Encoder();
        this._logger = Vue.logger;
    }

    discover() {
        return new Promise((resolve, reject) => {
            if (this._discovered) {
                resolve(this);
                return;
            }

            Vue.http.get(this._api.discovery).then(resp => {
                this._assignMethods(resp.body);
                this._discovered = true;

                this._logger.message(builder => {
                    builder.color('green').line({ action: 'discover', status: 'succeeded' })
                }).debug();
                resolve(this);
            }, resp => {
                this._logger.message(builder => {
                    builder.line({ action: 'discover', status: 'failed' })
                }).error();
                reject(resp);
            });
        });
    }

    _assignMethods(infos) {
        if (!Array.isArray(infos))
            Vue.logger.error('discover data is not an array', infos);

        infos.forEach(info => this._assignMethod(info));
    }

    _assignMethod(info) {
        const name = _.camelCase(info.name);
        const method = this._verifyHttpMethod(info.method);

        DiscoverApi.prototype[name] = (params) => {
            this._verifyParams(params, info);

            let debug_params = Object.assign({}, {}, params);
            let post_params = Object.assign({}, {}, params);
            Object.keys(debug_params).forEach(key => {
                const val = debug_params[key];
                if ((key === 'filter' || key === 'order') && typeof val === 'object') {
                    const json = this._jsonEncoder.encode(val);
                    const base64 = this._base64Encoder.encode(json);
                    debug_params[key] = { json, base64 };
                    post_params[key] = base64;
                }
            });

            const url = this._api.url(info.path);
            return new Promise((resolve, reject) => {
                Vue.http[method](url, { params: post_params }).then(resp => {
                    const error = resp.body.error;
                    if (typeof error != 'undefined' && error !== null && typeof error.code === 'number' && error.code !== 0) {
                        this._logger.message(builder => {
                            builder.line({ action: 'request', status: 'faield' })
                                .line({ url, method }).line({ params: debug_params }).line({ error });
                        }).error();
                        reject(error);
                        return;
                    }
                    resolve(resp.body.result);
                    this._logger.message(builder => {
                        builder.color('green').line({ action: 'request', status: 'succeeded' }).color()
                            .line({ url, method }).line({ params: debug_params }).line({ error });
                    }).debug();
                }, resp => {
                    this._logger.message(builder => {
                        builder.line({ action: 'request', status: 'faield' })
                            .line({ url, method }).line({ params: debug_params }).line({ error });
                    }).error();
                    reject(resp);
                });
            });
        };
    }

    _verifyHttpMethod(method) {
        if (typeof method === 'undefined' || method === null)
            throw new Error(`empty http method[method: ${method}]`);

        const methods = ['get', 'head', 'delete', 'jsonp', 'post', 'put', 'patch'];
        const methodName = method.toLowerCase();

        if (methods.findIndex(item => item === methodName) === -1)
            throw new Error(`invalid http method[method: ${method}]`);

        return methodName;
    }

    _verifyParams(params, info) {
        if (_.isUndefined(params) || _.isNull(params))
            return;

        info.parameters.filter(x => x.required).forEach(item => {
            const param = params[item.name];
            if (_.isUndefined(param) || _.isNull(param))
                throw new Error(`parameter [${item.name}] is required`);
        });
    }
}

export default DiscoverApi