import Vue from 'vue'
import _ from 'lodash'
import jwt from "jsonwebtoken";

class Auth {
    static defaults = {
        loginUrl: '',
        tokenName: 'token'
    };

    get loginUrl() { return this._options.loginUrl; }
    get tokenName() { return this._options.tokenName; }

    constructor(options) {
        this._options = _.extend({}, Auth.defaults, options);
        Vue.http.interceptors.push((request, next) => {
            let token = localStorage.getItem(this.tokenName);
            if (token) {
                let decoded = jwt.decode(token);
                if (decoded.exp > new Date().getTime()) {
                    localStorage.removeItem(this.tokenName);
                } else {
                    request.headers.set('Authorization', 'Bearer ' + token);
                }
            }
            next();
        });
    }

    login(credentials) {
        return Vue.http.get(this.loginUrl, { params: credentials }).then(resp => {
            localStorage.setItem(this.tokenName, resp.body);
            return resp.body;
        });
    }

    logout() {
        localStorage.removeItem(this.tokenName);
    }
}

export default Auth;