import Plugin from '../core/Plugin'

import Logger from '../core/Logging'
import Helper from './Helper'
import Auth from './Auth'
import API from './API'
import Builders from './Builders'

export default new Plugin().addServices({
    'logger': Logger,
    'helper': Helper,
    'auth': Auth,
    'api': API,
    'builders': Builders
});
