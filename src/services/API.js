import _ from 'lodash'
import DiscoverApi from './DiscoverApi'

class API {
    static defaults = {
        schema: 'http',
        host: '',
        port: 80,
        discovery: ''
    };

    get schema() { return this._options.schema; }
    get host() { return this._options.host; }
    get port() { return this._options.port === 80 ? '' : `:${this._options.port}`; }
    get discovery() { return this.url(this._options.discovery); }
    get game() { return this._game; }

    constructor(options) {
        this._options = _.extend({}, API.defaults, options);
        this._api = new DiscoverApi(this);
    }

    url(path) {
        return `${this.schema}://${this.host}${this.port}/${path.replace(/^\/+/, '')}`;
    }

    discover() {
        return this._api.discover();
    }
}

export default API