class QueryBuilder {
    static defaults = {
        params: {},
        default: {}
    }

    static item_defaults = {
        can: (key, values) => { return false; },
        generate: (key, values) => { return {}; }
    }

    constructor(options) {
        this._options = Object.assign({}, QueryBuilder.defaults, options);
        this._items = [];
    }

    add(item) {
        if (typeof item === 'undefined' || item === null) return this;
        this._items.push(Object.assign({}, QueryBuilder.item_defaults, item));
        return this;
    }

    addItems(items) {
        if (typeof items === 'undefined' || items === null) return this;
        items.forEach(item => this.add(item));
        return this;
    }

    builds() {
        const params = this._options.params;
        const rets = [];
        Object.keys(params).forEach(key => {
            const value = params[key];
            if (typeof value === 'undefined' || value === null || (Array.isArray(value) && value.length === 0))
                return;

            const values = Array.isArray(value) ? value : [value];
            const filter = this._items.find(filter => filter.can(key, values));
            if (typeof filter !== 'undefined' && filter !== null) {
                const ret = filter.generate(key, values);
                rets.push(ret);
            }
        });

        if (rets.length === 0) {
            const def = this._options.default;
            if (typeof def !== 'undefined' && def !== null) {
                rets.push(def);
            }
        }

        return rets;
    }
}

class FilterBuilder extends QueryBuilder {
    constructor(options) {
        super(options);
    }

    build() {
        const filters = this.builds();
        const len = filters.length;
        if (len === 0) return {};
        if (len === 1) return filters[0];
        return { $and: filters };
    }
}

class OrderBuilder extends QueryBuilder {
    constructor(options) {
        super(options);
    }

    build() {
        const orders = this.builds();
        let ret = {};
        orders.forEach(order => {
            ret = Object.assign({}, ret, order);
        });
        return ret;
    }
}

export {
    QueryBuilder,
    FilterBuilder,
    OrderBuilder
}