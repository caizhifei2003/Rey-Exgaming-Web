import { FilterBuilder, OrderBuilder } from './QueryBuilder'

export default {
    filter(options) {
        return new FilterBuilder(options);
    },
    order(options) {
        return new OrderBuilder(options);
    }
}