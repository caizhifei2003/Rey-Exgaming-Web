export default {
    toArray(value) {
        if (typeof value === 'undefined' || value === null) return [];
        if (Array.isArray(value)) return value.filter(x => typeof x !== 'undefined' && x !== null);
        return [value];
    }
}