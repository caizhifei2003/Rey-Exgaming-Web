import Vue from 'vue'
import Router from 'vue-router'

import Index from '@/pages/Index'
import Game from '@/pages/Game'

Vue.use(Router);

export default new Router({
  routes: [
    {
      name: 'index', path: '/', component: Index, meta: {
        scroll: {
          _items: [],
          on(item) { this._items.push(item); },
          emit() { while (this._items.length > 0) { this._items.pop()(...arguments); } }
        }
      }
    },
    { name: 'game', path: '/game/:id', component: Game }
  ],
  scrollBehavior(to, from, position) {
    if (!position) return undefined;

    const scroll = to.meta.scroll;
    if (typeof scroll === 'undefined' || scroll === null) return undefined;

    return new Promise((resolve, reject) => {
      scroll.on(() => resolve(position));
    });
  }
})
