import Plugin from '../core/Plugin'
import date from './date'

export default new Plugin()
    .addFilter('date', date)