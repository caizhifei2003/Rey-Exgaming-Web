class Plugin {
    get directives() { return this._directives; }
    get filters() { return this._filters; }
    get components() { return this._components; }
    get services() { return this._services; }

    constructor() {
        this._directives = {};
        this._filters = {};
        this._components = {};
        this._services = {};
    }

    addDirective(name, directive) {
        this._directives = Object.assign(this._directives, { [name]: directive });
        return this;
    }

    addDirectives(directives) {
        Object.keys(directives).forEach(name => {
            this.addDirective(name, directives[name]);
        });
        return this;
    }

    addFilter(name, filter) {
        this._filters = Object.assign(this._filters, { [name]: filter });
        return this;
    }

    addFilters(filters) {
        Object.keys(filters).forEach(name => {
            this.addFilter(name, filters[name]);
        });
        return this;
    }

    addComponent(name, component) {
        this._components = Object.assign(this._components, { [name]: component });
        return this;
    }

    addComponents(components) {
        Object.keys(components).forEach(name => {
            this.addComponent(name, components[name]);
        });
        return this;
    }

    addService(name, service) {
        this._services = Object.assign(this._services, { [name]: service });
        return this;
    }

    addServices(services) {
        Object.keys(services).forEach(name => {
            this.addService(name, services[name]);
        });
        return this;
    }

    addPlugin(name, plugin) {
        Object.keys(plugin.directives).forEach(name => {
            this.addDirective(name, plugin.directives[name]);
        });
        Object.keys(plugin.filters).forEach(name => {
            this.addFilter(name, plugin.filters[name]);
        });
        Object.keys(plugin.components).forEach(name => {
            this.addComponent(name, plugin.components[name]);
        });
        Object.keys(plugin.services).forEach(name => {
            this.addService(name, plugin.services[name]);
        });
        Object.assign(this, { [name]: plugin });
        return this;
    }

    addPlugins(plugins) {
        Object.keys(plugins).forEach(name => {
            this.addPlugin(name, plugins[name]);
        });
        return this;
    }

    install(Vue, options) {
        Object.keys(this._directives).forEach(name => {
            Vue.directive(name, this._directives[name]);
        });

        Object.keys(this._filters).forEach(name => {
            Vue.filter(name, this._filters[name]);
        });

        Object.keys(this._components).forEach(name => {
            Vue.component(name, this._components[name]);
        });

        Object.keys(this._services).forEach(name => {
            var type = typeof (this._services[name]);
            if (type === 'function') {
                Vue[name] = Vue.prototype['$' + name] = new this._services[name](options ? options[name] : undefined);
            } else if (type === 'object') {
                Vue[name] = Vue.prototype['$' + name] = this._services[name];
            }
        });
    }
}

export default Plugin;