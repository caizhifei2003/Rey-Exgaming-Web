import Encoder from './Encoder'
import moment from 'moment'

class MongoJsonEncoder extends Encoder {
    encode(value) {
        if (typeof value === 'undefined' || value === null) return '{}';
        if (typeof value === 'string') return `"${value}"`;
        let pairs = [];
        _.forEach(value, (v, k) => {
            let strv = null;
            const type = typeof v;
            if (type === 'string') {
                strv = `"${v.replace(/[\\\"]/ig, (x) => ('\\' + x))}"`;
            } else if (type === 'number' || type === 'boolean' || v === null) {
                strv = `${v}`;
            } else if (type === 'undefined' || type === 'function') {
                //! do nothing.
            } else if (v instanceof Date) {
                strv = `new Date("${moment(v).format('YYYY-MM-DD HH:mm:ss')}")`;
            } else if (Array.isArray(v)) {
                strv = `[${v.map(item => this.encode(item)).join(',')}]`;
            } else {
                strv = this.encode(v);
            }

            if (strv !== null) {
                pairs.push(`"${k}":${strv}`);
            }
        });

        if (pairs.length > 0) {
            return `{${pairs.join(',')}}`;
        }
        return '{}';
    }
}

export default MongoJsonEncoder