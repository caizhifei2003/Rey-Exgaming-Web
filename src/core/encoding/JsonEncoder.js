import Encoder from './Encoder'
import _ from 'lodash'
import moment from 'moment'

class JsonEncoder extends Encoder {
    encode(value) {
        return JSON.stringify(value);
    }

    // {"$and":[{"publishDate":{"$gt":new Date("2017-12-21 19:26:53")}},{"tags":{"$in":["色情内容","暴力","竞速","网络出版","角色扮演"]}}]}
    decode(content) {
        return JSON.parse(content, (key, value) => {
            //2017-12-21T11:41:15.035Z
            if (/^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}\.\d{3}Z$/.test(value)) {
                return new Date(value);
            }
            return value;
        });
    }
}

export default JsonEncoder;