import Encoder from './Encoder'
import JsonEncoder from './JsonEncoder'
import Base64Encoder from './Base64Encoder'
import _ from 'lodash'

class StatusEncoder extends Encoder {
    constructor() {
        super();
        this._json = new JsonEncoder();
        this._base64 = new Base64Encoder();
    }

    encode(value) {
        return this._base64.encode(this._json.encode(value));
    }

    decode(content) {
        if (_.isUndefined(content))
            return {};

        return this._json.decode(this._base64.decode(content));
    }
}

export default StatusEncoder