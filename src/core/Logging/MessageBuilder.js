import ConsoleLogger from './ConsoleLogger'

class MessageBuilder {
    constructor(option) {
        this._option = option;
        this._colors = [];
        this._content = '';

        this._option.fields.forEach(({ name, value }) => {
            this.field(name, value);
            this.newline();
        });
    }

    append(content) {
        this._content += content;
        return this;
    }

    color(value) {
        this._colors.push(`color:${value || 'black'}`)
        return this.append('%c');
    }

    field(name, value) {
        if (typeof value === 'function') {
            value = value();
        }
        if (typeof value === 'object') {
            value = JSON.stringify(value);
        }
        return this.append(`[${name}:${value}]`);
    }

    fields(values) {
        Object.keys(values).forEach(key => this.field(key, values[key]));
        return this;
    }

    newline() {
        return this.append('\n');
    }

    line(value) {
        if (typeof value === 'string') {
            this.append(value);
        } else if (typeof value === 'object') {
            this.fields(value);
        }
        return this.newline();
    }

    build() {
        return new ConsoleLogger(this._option, this._content, this._colors);
    }
}

export default MessageBuilder