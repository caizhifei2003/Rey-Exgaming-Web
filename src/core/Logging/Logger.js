import LoggerOptionBuilder from './LoggerOptionBuilder'
import MessageBuilder from './MessageBuilder'


class Logger {
    constructor(build) {
        const builder = new LoggerOptionBuilder();
        if (typeof build === 'function') {
            build(builder);
        }
        this._option = builder.build();
    }

    message(build) {
        const builder = new MessageBuilder(this._option);
        if (typeof build === 'function') {
            build(builder);
        }
        return builder.build();
    }
}

export default Logger