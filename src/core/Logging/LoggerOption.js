class LoggerOption {
    get levels() { return this._levels; }
    get fields() { return this._fields; }

    constructor({ levels, fields }) {
        this._levels = levels;
        this._fields = fields;
    }

    hasLevel(level) {
        return this._levels.findIndex(x => x === level) > -1;
    }
}

export default LoggerOption