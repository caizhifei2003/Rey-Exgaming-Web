class ConsoleLogger {
    get log() {
        if (this._option.hasLevel('log'))
            return console.log.bind(console, this._content, ...this._colors);
        return this._empty;
    }

    get info() {
        if (this._option.hasLevel('info'))
            return console.info.bind(console, this._content, ...this._colors);
        return this._empty;
    }

    get debug() {
        if (this._option.hasLevel('debug'))
            return console.debug.bind(console, this._content, ...this._colors);
        return this._empty;
    }

    get warn() {
        if (this._option.hasLevel('warn'))
            return console.warn.bind(console, this._content, ...this._colors);
        return this._empty;
    }

    get error() {
        if (this._option.hasLevel('error'))
            return console.error.bind(console, this._content, ...this._colors);
        return this._empty;
    }

    constructor(option, content, colors) {
        this._option = option;
        this._content = content;
        this._colors = colors || [];
        this._empty = () => { };
    }
}

export default ConsoleLogger