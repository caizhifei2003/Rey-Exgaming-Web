import LoggerOption from './LoggerOption'

const __levels = ['log', 'info', 'debug', 'warn', 'error'];
const __levels_development = __levels;
const __levels_production = ['error'];

class LoggerOptionBuilder {
    constructor() {
        this._levels = [];
        this._fields = [];
    }

    level(value) {
        if (typeof value !== 'string')
            return this;

        if (value === 'all') {
            this._levels = __levels;
            return this;
        } else if (value === 'development') {
            this._levels = __levels_development;
            return this;
        } else if (value == 'production') {
            this._levels = __levels_production;
            return this;
        }

        if (!__levels.find(x => x === value))
            throw new Error(`invalid level value ${value}`);

        this._levels.push(value);
        return this;
    }

    field(name, value) {
        this._fields.push({ name, value });
        return this;
    }

    fields(values) {
        Object.keys(values).forEach(key => this.field(key, values[key]));
        return this;
    }

    build() {
        return new LoggerOption({
            levels: this._levels,
            fields: this._fields
        });
    }
}

export default LoggerOptionBuilder